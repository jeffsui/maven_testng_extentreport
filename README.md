# maven_testng_extentreport integration

## technical 

### 1. Core Java file

[ExtentTestNGIReporterListener.java](https://gitlab.com/jeffsui/maven_testng_extentreport/-/blob/master/src/test/java/org/asjy/app/report/extension/ExtentTestNGIReporterListener.java)

### 2.`pom.xml` file

```xml
<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.22.2</version>
				<configuration>
					<properties>
						<property>
							<name>usedefaultlistener</name>
							<value>false</value>
						</property>
					</properties>
					<workingDirectory>target/</workingDirectory>
					<suiteXmlFiles>
						<suiteXmlFile>testng.xml</suiteXmlFile>
					</suiteXmlFiles>
				</configuration>
			</plugin>
		</plugins>
	</build>
	<dependencies>
		<dependency>
			<groupId>org.testng</groupId>
			<artifactId>testng</artifactId>
			<version>6.14.3</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.relevantcodes</groupId>
			<artifactId>extentreports</artifactId>
			<version>2.41.2</version>
		</dependency>

		<dependency>
			<groupId>com.vimalselvam</groupId>
			<artifactId>testng-extentsreport</artifactId>
			<version>1.3.1</version>
		</dependency>

		<dependency>
			<groupId>com.aventstack</groupId>
			<artifactId>extentreports</artifactId>
			<version>3.1.5</version>
		</dependency>

	</dependencies>

```

## follow these below steps:

### 1. export from maven project 
### 2. run `mvn test`
### 3. check target/test-output folder

![](snapshot/snapshot_20200314_testng_report.png)

## issues 
if you have any issues,please Contact 215687736@qq.com ,Thanks a lot.