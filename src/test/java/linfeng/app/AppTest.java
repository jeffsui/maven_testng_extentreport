package linfeng.app;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

	
    @Test
    public void test01(){
        Assert.assertEquals(2,2);
    }

    @Test
    public void test02(){
        Assert.assertEquals(1,1);
    }

    @Test
    public void test03(){
        Assert.assertEquals("aaa","aaa");
    }

    @Test
    public void logDemo(){
        Reporter.log("这是我们自己写的日志");
        //throw new RuntimeException("这我我们自己拋的异常~~~~");
    }
}

